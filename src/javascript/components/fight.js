
export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const damage =  getHitPower(attacker) - getBlockPower(defender);
  if(damage < 0){
    return 0;
  }
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance  = Math.random() + 1;
  return fighter.attack * criticalHitChance ;
}

export function getBlockPower(fighter) {
  const dodgeChance   = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

function getCriticalDamage(fighter) {
  return fighter.attack * 2;
}