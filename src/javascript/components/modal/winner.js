import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const imgFighter = createFighterImage(fighter);
  showModal({
    title:  `${fighter.name} Winner!!!`,
    bodyElement: imgFighter,
    onClose: () => { location.reload(); }
  });
}
