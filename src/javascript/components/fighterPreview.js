import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(fighter!==undefined){
    const fighterImage = createElement({
      tagName: 'img',
      attributes: { src: fighter.source},
      className:'preview-container___versus-img'
    });

    const fighterInfo = createElement({
      tagName:'div'
    });

    fighterInfo.innerHTML = `<p style='color: white'>Name: ${fighter.name}</p>
      <p style='color: white'>Health: ${fighter.health}</p>
      <p style='color: white'>Defense: ${fighter.defense}</p>
      <p style='color: white'>Attack: ${fighter.attack}</p>`
    fighterElement.append(fighterImage);
    fighterElement.append(fighterInfo);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
